import { makepuzzle, solvepuzzle } from 'sudoku';
import { chunkArrayIntoObject, unchunkObjectIntoArray } from '../utils/array-utils';
import { RESOLVE_AUTO_SUDOKU, VERIFY_SUDOKU_SOLUTION, UPDATE_SUDOKU_GRID } from '../constants/SudokuActionTypes';

const sudokuGrid = chunkArrayIntoObject(makepuzzle());
const sudokuSolution = chunkArrayIntoObject(solvepuzzle(unchunkObjectIntoArray(sudokuGrid)));

const initialState = {
  sudokuGrid,
  sudokuSolution,
  isWin: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case RESOLVE_AUTO_SUDOKU:
      return {
        ...state,
        sudokuGrid: state.sudokuSolution,
      };

    case VERIFY_SUDOKU_SOLUTION:
      return {
        ...state,
        isWin:
        JSON.stringify(unchunkObjectIntoArray(state.sudokuGrid)) 
        === JSON.stringify(unchunkObjectIntoArray(state.sudokuSolution)),
      };

    case UPDATE_SUDOKU_GRID:
      return {
        ...state,
        sudokuGrid: action.sudokuGrid,
      };

    default:
      return state;
  }
}

export default reducer;
