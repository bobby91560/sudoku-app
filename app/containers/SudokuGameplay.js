import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getSudokuGrid, getSudokuResult } from '../selectors';
import * as SudokuActions from '../actions/actions';
import SudokuGame from '../components/SudokuGame';

const mapStateToProps = (state) => ({
  puzzle: getSudokuGrid(state),
  isWin: getSudokuResult(state),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(SudokuActions, dispatch),
});

const SudokuGameplay = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SudokuGame);

export default SudokuGameplay;
