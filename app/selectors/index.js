import { createSelector } from 'reselect';

const sudokuGrid = (state) => state.sudoku.sudokuGrid;
const sudokuResult = (state) => state.sudoku.isWin;

export const getSudokuGrid = createSelector(
  [sudokuGrid],
  (puzzle) => puzzle,
);

export const getSudokuResult = createSelector(
  [sudokuResult],
  (isWin) => isWin,
);
