import React from 'react';
import { render, cleanup } from "@testing-library/react";
import { Hello } from '../../../components/index';

afterEach(cleanup);

it("should have a title 'Hello World'", () => {
    const message = 'Hello World';
    const { getByText } = render(<Hello />);
    expect(getByText(message)).toBeDefined();
    expect(getByText(message).textContent).toBe(message);
    expect(getByText(message).tagName).toBe('H1');
});