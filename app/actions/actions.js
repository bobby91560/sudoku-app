import * as types from '../constants/SudokuActionTypes';

export const resolveSudoku = () => ({
  type: types.RESOLVE_AUTO_SUDOKU,
});

export const verifySudokuSolution = () => ({
  type: types.VERIFY_SUDOKU_SOLUTION,
});

export const updateGrid = (sudokuGrid) => ({
  type: types.UPDATE_SUDOKU_GRID,
  sudokuGrid: { ...sudokuGrid },
});
