export const chunkArrayIntoObject = (arrayToChunk, chunkSize = 9) => {
  const results = [];
  while (arrayToChunk.length) {
    results.push(arrayToChunk.splice(0, chunkSize));
  }
  return { ...results };
};

export const unchunkObjectIntoArray = (objectToUnchunck) => {
  const results = [];
  Object.keys(objectToUnchunck).map((item) => {
    objectToUnchunck[item].forEach((value) => {
      results.push(value);
    });
  });
  return results;
}