import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import SudokuGameplay from './containers/SudokuGameplay';
import store from './store';
import './style.scss';

const App = () => (
  <Provider store={store}>
    <SudokuGameplay />
  </Provider>
);
ReactDOM.render(<App />, document.getElementById('app'));
