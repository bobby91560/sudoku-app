/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import SudokuTable from './SudokuTable/SudokuTable';

const SudokuGame = ({
  puzzle, isWin, actions,
}) => (
  <div>
    <SudokuTable
      sudokuBoard={puzzle}
      actions={actions}
    />
    <button type="button" onClick={actions.resolveSudoku}>Résoudre automatiquement le sudoku</button>
    <button type="button" onClick={actions.verifySudokuSolution}>Vérifier si vous avez gagné</button>
    {isWin
      && (
      <div>
        <p>Vous avez gagné</p>
      </div>
      )}
  </div>
);

SudokuGame.propTypes = {
  puzzle: PropTypes.object.isRequired,
  isWin: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
};

export default SudokuGame;
