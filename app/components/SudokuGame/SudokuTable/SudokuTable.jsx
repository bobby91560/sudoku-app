/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import SudokuCell from '../SudokuCell';
import './SudokuTable.scss';

const SudokuTable = ({ sudokuBoard, actions }) => {
  const { updateGrid } = actions;
  const sudokuBoardGame = Object.keys(sudokuBoard).map((sudokuRow, indexRow) => {
    const sudokuCell = Object.keys(sudokuBoard[sudokuRow]).map((valueCell, indexCell) => (
      <SudokuCell
        sudokuBoard={sudokuBoard}
        value={
          sudokuBoard[sudokuRow][valueCell] !== null
            ? sudokuBoard[sudokuRow][valueCell] + 1
            : null
        }
        key={`${indexRow}-${indexCell}`}
        sudokuRow={indexRow}
        sudokuCell={indexCell}
        updateGrid={updateGrid}
      />
    ));
    return (
      <div className="sudoku-row" key={indexRow}>
        {sudokuCell}
      </div>
    );
  });

  return (
    <div className="sudoku-table">
      {sudokuBoardGame}
    </div>
  );
};

SudokuTable.propTypes = {
  sudokuBoard: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
};

export default SudokuTable;
