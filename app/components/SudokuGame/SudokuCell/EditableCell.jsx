import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

const EditableCell = ({
  value,
  changeValueCell,
  validateChangeValue,
}) => {
  const editableCellRef = useRef();

  useEffect(() => {
    editableCellRef.current.focus();
  }, []);

  return (
    <input
      ref={editableCellRef}
      type="text"
      defaultValue={value}
      onChange={changeValueCell}
      onKeyDown={validateChangeValue}
      onBlur={validateChangeValue}
    />
  );
};

EditableCell.propTypes = {
  value: PropTypes.string,
  changeValueCell: PropTypes.func.isRequired,
  validateChangeValue: PropTypes.func.isRequired,
};

EditableCell.defaultProps = {
  value: null,
};

export default EditableCell;
