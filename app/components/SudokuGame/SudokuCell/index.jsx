/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import EditableCell from './EditableCell';
import SimpleCell from './SimpleCell';
import './SudokuCell.scss';

const SudokuCell = ({
  value, sudokuBoard, sudokuCell, sudokuRow, updateGrid,
}) => {
  const [valueCell, setValueCell] = useState(value);
  const [initValueCell] = useState(value);
  const [isEditableCell, setEditableCell] = useState(false);
  const maxLengthValueCell = 1;
  const readOnlyCell = initValueCell !== null;

  useEffect(() => {
    setValueCell(value);
  }, [value]);

  const editCell = (event) => {
    event.preventDefault();
    setEditableCell(true);
  };

  const changeValueCell = (event) => {
    if (
      (!Number.isNaN(event.target.value) || event.target.value === '')
        && event.target.value.length <= maxLengthValueCell
    ) {
      setValueCell(event.target.value);
    } else {
      // eslint-disable-next-line no-param-reassign
      event.target.value = `${valueCell}`;
    }
  };

  const updateSudokuBoard = (valueToUpdate) => {
    const sudokuBoardToUdpate = { ...sudokuBoard };
    sudokuBoardToUdpate[sudokuRow][sudokuCell] = valueToUpdate;
    updateGrid(sudokuBoardToUdpate);
  };

  const validateChangeValue = (event) => {
    if (event.key === 'Enter' || event.type === 'blur') {
      const changedCellValue = event.target.value !== '' ? parseInt(event.target.value, 10) - 1 : null;
      if (changeValueCell !== null) {
        updateSudokuBoard(changedCellValue);
        setEditableCell(false);
      }
    }
  };

  const makeSquareStyle = () => {
    const realSudokuCell = sudokuCell + 1;
    const realSudokuRow = sudokuRow + 1;
    const squareStyle = {};

    if (realSudokuCell % 3 === 0 && realSudokuCell !== 9) {
      Object.assign(squareStyle, { borderRight: '2px solid black' });
    }

    if (realSudokuRow % 3 === 0 && realSudokuRow !== 9) {
      Object.assign(squareStyle, { borderBottom: '2px solid black' });
    }

    return squareStyle;
  };

  return (
    <div
      className={`sudoku-cell${readOnlyCell ? ' readonly-cell' : ''}`}
      onClick={readOnlyCell ? undefined : editCell}
      style={makeSquareStyle()}
    >
      { isEditableCell
        ? (
          <EditableCell
            changeValueCell={changeValueCell}
            validateChangeValue={validateChangeValue}
            value={valueCell}
          />
        )
        : (
          <SimpleCell
            value={valueCell}
          />
        )}
    </div>
  );
};

SudokuCell.propTypes = {
  value: PropTypes.number,
  sudokuBoard: PropTypes.object.isRequired,
  sudokuCell: PropTypes.number.isRequired,
  sudokuRow: PropTypes.number.isRequired,
  updateGrid: PropTypes.func.isRequired,
};

SudokuCell.defaultProps = {
  value: null,
};

export default SudokuCell;
