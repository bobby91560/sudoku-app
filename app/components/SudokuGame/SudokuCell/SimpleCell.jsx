import React from 'react';
import PropTypes from 'prop-types';

const SimpleCell = ({ value }) => (
  <p>{value}</p>
);

SimpleCell.propTypes = {
  value: PropTypes.number,
};

SimpleCell.defaultProps = {
  value: null,
};

export default SimpleCell;
